angular.module("fazioApp", ["ngRoute"])
    .controller("LoginController", function($scope, $http, $location, $window){

        $scope.apiGrantCodeUrl = "http://api.fazio.sensedia.com/oauth/grant-code";

        $scope.grantCodeBody = {
            client_id: "",
            extraInfo: {},
            redirect_uri: "http://" + $window.location.hostname + ":9090/pages/index.html",
            state: ""
        }

        $scope.submitLogin = function(){

            var successCallback = function(result){
                var url = result.data.redirect_uri + "&clientId=" +  $scope.grantCodeBody.client_id + "&clientSecret=" + $scope.clientSecret;

                $window.open(url, "_blank");
            }

            var errorCallback = function(result){

            }

            var params = $window.location.search.split("=");

            $scope.grantCodeBody.client_id = params[1].replace("&clientSecret", "");
            $scope.clientSecret = params[2];

            $http.post($scope.apiGrantCodeUrl, $scope.grantCodeBody)
                  .then(successCallback, errorCallback);

        }
    });